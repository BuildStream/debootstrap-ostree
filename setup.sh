#!/bin/bash
#
#  Copyright (C) 2017 Codethink Limited
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public
#  License as published by the Free Software Foundation; either
#  version 2 of the License, or (at your option) any later version.
#
#  This library is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
#  Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public
#  License along with this library. If not, see <http://www.gnu.org/licenses/>.
#
#  Authors:
#        Tristan Van Berkom <tristan.vanberkom@codethink.co.uk>
topdir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

# default options
arg_workdir=${topdir}/work
arg_mirror="http://ftp.us.debian.org/debian/"
arg_schedule=
arg_setup_apache=false
arg_gpg=
arg_gpg_home=

function usage () {
    echo "Usage: "
    echo "  setup.sh [OPTIONS]"
    echo
    echo "Setup a machine for debootstrapping into an ostree repo"
    echo
    echo "Options:"
    echo
    echo "  -h --help                    Display this help message and exit"
    echo "  -w --workdir  <directory>    The directory to perform builds in (default: 'work' subdirectory)"
    echo "  -s --schedule <expression>   A cron expression indicating when unconditional builds should run (default: no cron jobs)"
    echo "  -m --mirror   <uri>          A debian mirror uri (defaults to USA mirror)"
    echo "  -g --gpg      <gpg>          A GPG key to sign the ostree commits with"
    echo "  --gpg-home    <directory>    A GPG home directory, e.g. /home/user/.gnupg"
    echo "  --setup-apache               Setup the apache server to host the ostree repo"
}

while : ; do
    case "$1" in
	-h|--help)
	    usage;
	    exit 0;
	    shift ;;

	-w|--workdir)
	    arg_workdir=${2}
	    shift 2 ;;

	-s|--schedule)
	    arg_schedule=${2}
	    shift 2 ;;

	-m|--mirror)
	    arg_mirror=${2};
	    shift 2 ;;

	-g|--gpg)
	    arg_gpg=${2};
	    shift 2 ;;

	--gpg-home)
	    arg_gpg_home=${2};
	    shift 2 ;;

	--setup-apache)
	    arg_setup_apache=true
	    shift ;;

	*)
	    break ;;
    esac
done

#
# Some sanity checks and path resolutions
#
mkdir -p "${arg_workdir}" || dienow "Failed to create work directory: ${arg_workdir}"
mkdir -p "${arg_workdir}/export"

arg_workdir="$(cd ${arg_workdir} && pwd)"

function generateLaunchers () {
    local scriptname=

    # Create the multistrap.conf with our preferred mirror
    #
    sed -e "s|@@MIRROR@@|${arg_mirror}|g" \
	${topdir}/data/multistrap.conf.in > ${topdir}/multistrap.conf

    # Create the launch script based on our current configuration
    # and ensure that there is an entry in the user's crontab for
    # the launcher.
    #
    for arch in i386 amd64 armhf arm64; do
	scriptname="launcher-${arch}.sh"

	sed -e "s|@@WORKDIR@@|${arg_workdir}|g" \
            -e "s|@@TOPDIR@@|${topdir}|g" \
            -e "s|@@ARCH@@|${arch}|g" \
	    ${topdir}/data/launcher.sh.in > ${topdir}/${scriptname}

	if [ -n "${arg_gpg}" -a -n "${arg_gpg_home}" ]; then
	    echo "*${arg_gpg}*${arg_gpg_home}*"
	    arg_gpg="--gpg ${arg_gpg}"
	    arg_gpg_home="--gpg-home ${arg_gpg_home}"
	else
	    arg_gpg=
	    arg_gpg_home=
	fi

	sed -e "s|@@GPGKEY@@|${arg_gpg}|g" \
	    -e "s|@@GPGHOME@@|${arg_gpg_home}|g" \
	    -i ${topdir}/${scriptname}

	chmod +x ${topdir}/${scriptname}
    done
}

# Ensure the build schedule for either unconditional
# or continuous builds.
#
#  $1 - "continuous" or "unconditional"
#
function ensureBuildSchedule () {
    local scriptname=
    local job=

    for arch in i386 amd64 armhf arm64; do
	scriptname="launcher-${arch}.sh"
	job="${arg_schedule} ${topdir}/${scriptname}"

	# Register the job with user's crontab
	cat <(fgrep -i -v "${scriptname}" <(crontab -l)) <(echo "$job") | crontab -
    done
}

function configureApache () {
    apache_data="${topdir}/data/apache"
    apache_dir="/etc/apache2"
    apache_conf="${apache_dir}/apache2.conf"
    apache_site="${apache_dir}/sites-available/000-default.conf"
    apache_ssl="${apache_dir}/sites-available/default-ssl.conf"
    export_dir="${arg_workdir}/export"

    if [ ! -f "${apache_conf}" ] || [ ! -f "${apache_conf}" ] || [ ! -f "${apache_ssl}" ]; then
	echo "Unrecognized apache server; not setting up apache"
	return
    fi

    # Configure apache to serve our build results
    #
    sed -e "s|@@SITE_ROOT@@|${export_dir}|g" ${apache_data}/apache2.conf.in     | sudo tee ${apache_conf} > /dev/null
    sed -e "s|@@SITE_ROOT@@|${export_dir}|g" ${apache_data}/000-default.conf.in | sudo tee ${apache_site} > /dev/null
    sed -e "s|@@SITE_ROOT@@|${export_dir}|g" ${apache_data}/default-ssl.conf.in | sudo tee ${apache_ssl}  > /dev/null

    # Restart with new config
    #
    echo "Restarting apache server to serve build results at: ${export_dir}"
    sudo service apache2 restart
}

#
# Main
#

generateLaunchers

# Schedule or change schedule of the nightlies 
if [ ! -z "${arg_schedule}" ]; then
    ensureBuildSchedule
fi

# Automatically squash the system apache configuration
# to serve the export directory
if $arg_setup_apache; then
    configureApache
fi
