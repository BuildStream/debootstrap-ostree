debootstrap-ostree
==================
This repository contains a script which produces an ostree repository
by running debootstrap and committing the results to the ostree repository.

The dependency list used to create the repository is based on the
GNOME system dependencies, so that GNOME can be built on top of
a checkout of the resulting ostree.

Additionally this contains a setup.sh script for installing a cron
job to run the debootstrapping regularly, and has an option for
setting up an apache server to serve the logs and the ostree repo.
